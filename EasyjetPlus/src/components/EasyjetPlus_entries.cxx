#include "EasyjetPlus/PostProcessor.h"
#include "../DummyPostProcessTool.h"
#include "../SumOfWeightsTool.h"
#include "../GetXSectionTool.h"
#include "../TotalWeightsTool.h"

DECLARE_COMPONENT(PostProcessor)
DECLARE_COMPONENT(DummyPostProcessTool)
DECLARE_COMPONENT(SumOfWeightsTool)
DECLARE_COMPONENT(GetXSectionTool)
DECLARE_COMPONENT(TotalWeightsTool)
